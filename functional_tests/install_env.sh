#!/bin/bash
echo "Will install ENVIRONMENT\n"
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv 7F0CEB10 &&
echo "deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen" >> ./10gen.list &&
sudo mv ./10gen.list  /etc/apt/sources.list.d/10gen.list &&
sudo apt-get update &&
sudo apt-get install mongodb-10gen &&
wget http://nodejs.org/dist/v0.10.1/node-v0.10.1-linux-x86.tar.gz &&
tar xvzf node-v0.10.1-linux-x86.tar.gz;
export PATH=`pwd`/node-v0.10.1-linux-x86/bin:$PATH