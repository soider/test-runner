"""This module contains some assert specific functions"""
import sys
sys.path.append("..")
import os
import pymongo
import time
import pprint
from bson.objectid import ObjectId

from tools.mongo import get_test_database, drop_test_database
from tools.asserts import check_gdclick, check_gdsource, \
    check_ai, check_apid, check_subid, \
    check_clickid, assert_eq, check_price_and_currency,\
    check_aprice_and_currency
p = pprint.pprint
database = get_test_database("reciever")


def base_assert(case):
    """Enter point"""
    try:
        time.sleep(2)
        result = case["result"]
        assert result.headers["Content-Type"] == "image/png"
        assert result.getcode() == 200
        assert result.read().strip() == ''
        last_insert = database.rawactions.find_one()
        if last_insert is None:
            raise Exception("There was no insert in db")
        tariff = database.tariffs.find_one(
            {"_id": ObjectId(case["config"]["values"]["ai"])})
        if tariff is None:
            raise Exception("There is no corresponding tariff in mongo")
        currencies = database.currencies.find()
        if currencies is None:
            raise Exception("There is no currencies in mongo")
        check_gdclick(case, last_insert)
        check_gdsource(case, last_insert)
        check_ai(case, last_insert)
        if "apid" in case["config"]["values"]:
            check_apid(case, last_insert)
        if "subid" in case["config"]["values"]:
            check_subid(case, last_insert)
        if "clickId" in case["config"]["values"]:
            check_clickid(case, last_insert)
        if "aprice" in case["config"]["values"]:
            check_aprice_and_currency(
                case, last_insert, tariff, list(currencies))
        if "price" in case["config"]["values"]:
            check_price_and_currency(
                case, last_insert, tariff, list(currencies))
    except Exception, er:
        raise er
    finally:
        # Always drop collection
        database.rawactions.drop()
