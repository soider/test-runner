# *-* encoding: utf-8 *-*
"""This module contains some functionality, running before testsuite"""
import sys
sys.path.append("..")  # dirty hack while no setup.py
import os

from tools.fixtures import generate_tmp_config
from tools.fixtures import start_goodvert, stop_node_js

from bson.objectid import ObjectId
from tools.mongo import get_test_database, drop_test_database

database = get_test_database("reciever")


def load_fixtures():
    """Inserts some test data need to correct functional cases"""
    password_object = [{"_id": ObjectId("50f9203c210fdd671800000c"),
                       "passw": None},
                       {"_id": ObjectId("50f9203c210fdd671800000b"),
                       "passw": "password"}
                       ]
    offer = [{
        "__v": 2,
        "_id": ObjectId("50f9203c210fdd671800000c"),
        "advertiser": ObjectId("50f52c0a11a145375800001f"),
        "campaign_warning": "",
        "category": ObjectId("50a4a74ae4212acf562a4cff"),
        "conf": {
            "hasPixel": True,
            "s2s": False,
            "workScript": ""
        },
        "description": " description 41",
        "dont_track_status": False,
        "geolocations": [
            3031,
            3030
        ],
        "hold": "30",
        "inputUrl": "http://dont-go-here.com/{$subpage}?webmasterid=\
{$webmasterId}&source={$source}&clickId={$click_id}",
        "link": "http://www.link41",
        "locations": [
            ObjectId("50a4a739e4212acf562a4cf8"),
            ObjectId("50a4a739e4212acf562a4cf9")
        ],
        "logo": "/uploads/50accb44d80869ac22000002/Seafight_logo1.png",
        "name": "offer 41",
        "postclick": 365,
        "promo": "/uploads/50accb44d80869ac22000002/Seafight_logo1.rar",
        "statusUrl": "http://www.statusUrl41.ru/",
        "tariffs": [
            ObjectId("50f544d311a1453758000021")
        ],
        "trafficSourceTypes": [
            ObjectId("50a4a757e4212acf562a4d00"),
            ObjectId("50a4a757e4212acf562a4d02"),
            ObjectId("50a4a757e4212acf562a4d04"),
            ObjectId("50a4a757e4212acf562a4d05"),
            ObjectId("50a4a757e4212acf562a4d06"),
            ObjectId("50a4a757e4212acf562a4d07")
        ],
        "unhold": False,
        "visible": True,
        "warning": "warning 41"
    },
    {
        "__v": 2,
        "_id": ObjectId("50f9203c210fdd671800000b"),
        "advertiser": ObjectId("50f52c0a11a145375800001f"),
        "campaign_warning": "",
        "category": ObjectId("50a4a74ae4212acf562a4cff"),
        "conf": {
            "hasPixel": True,
            "s2s": True,
            "workScript": ""
        },
        "description": " description 41",
        "dont_track_status": False,
        "geolocations": [
            3031,
            3030
        ],
        "hold": "30",
        "inputUrl": "http://dont-go-here.com/{$subpage}?webmasterid=\
{$webmasterId}&source={$source}&clickId={$click_id}",
        "link": "http://www.link41",
        "locations": [
            ObjectId("50a4a739e4212acf562a4cf8"),
            ObjectId("50a4a739e4212acf562a4cf9")
        ],
        "logo": "/uploads/50accb44d80869ac22000002/Seafight_logo1.png",
        "name": "offer 41",
        "postclick": 365,
        "promo": "/uploads/50accb44d80869ac22000002/Seafight_logo1.rar",
        "statusUrl": "http://www.statusUrl41.ru/",
        "tariffs": [
            ObjectId("50f544d311a1453758000021")
        ],
        "trafficSourceTypes": [
            ObjectId("50a4a757e4212acf562a4d00"),
            ObjectId("50a4a757e4212acf562a4d02"),
            ObjectId("50a4a757e4212acf562a4d04"),
            ObjectId("50a4a757e4212acf562a4d05"),
            ObjectId("50a4a757e4212acf562a4d06"),
            ObjectId("50a4a757e4212acf562a4d07")
        ],
        "unhold": False,
        "visible": True,
        "warning": "warning 41"
    },
    {
        "__v": 2,
        "_id": ObjectId("50f9203c210fdd6718cccccc"),
        "advertiser": ObjectId("50f52c0a11a145375800001f"),
        "campaign_warning": "",
        "category": ObjectId("50a4a74ae4212acf562a4cff"),
        "conf": {
            "hasPixel": True,
            "s2s": False,
            "workScript": ""
        },
        "description": " description 41",
        "dont_track_status": False,
        "geolocations": [
            3031,
            3030
        ],
        "hold": "30",
        "inputUrl": "http://dont-go-here.com/{$subpage}?webmasterid=\
{$webmasterId}&source={$source}&clickId={$click_id}",
        "link": "http://www.link41",
        "locations": [
            ObjectId("50a4a739e4212acf562a4cf8"),
            ObjectId("50a4a739e4212acf562a4cf9")
        ],
        "logo": "/uploads/50accb44d80869ac22000002/Seafight_logo1.png",
        "name": "offer 41",
        "postclick": 365,
        "promo": "/uploads/50accb44d80869ac22000002/Seafight_logo1.rar",
        "statusUrl": "http://www.statusUrl41.ru/",
        "tariffs": [
            ObjectId("50f544d311a1453758000021")
        ],
        "trafficSourceTypes": [
            ObjectId("50a4a757e4212acf562a4d00"),
            ObjectId("50a4a757e4212acf562a4d02"),
            ObjectId("50a4a757e4212acf562a4d04"),
            ObjectId("50a4a757e4212acf562a4d05"),
            ObjectId("50a4a757e4212acf562a4d06"),
            ObjectId("50a4a757e4212acf562a4d07")
        ],
        "unhold": False,
        "visible": True,
        "warning": "warning 41"
    }]
    campaign =\
        {"__v": 0,
         "_id": ObjectId("5100eed2f7eacf0a75000027"),
         "name": "Диеты 25 кадр 1",
         "offer": ObjectId("5079424634fcc59bc77ef48e"),
         "owner": ObjectId("5055a5a805340e6c3b000001"),
         "status": "active",
         "trafficBack": "http://ya.ru",
         "trafficSource": ObjectId("50a0aff03414485243000002"),
         "type": "redirect",
         "workLink": "http://test?0.26159041514620185"
         }

    tarif_object = [
        {"__v": 0,
         "_id": ObjectId("50f9203c210fdd6718ffffff"),
         "actionType": {
             "_id": ObjectId("50a4a722e4212acf562a4cf5"),
             "name": "Оформление заказа"
         },
         "currency": {
             "name": "USD"
         },
         "jscode": "http://www.yandex.ru/",
         "offer": ObjectId("50f9203c210fdd6718cccccc"),
         "rateadvert": "10",
         "ratepartner": "15",
         "status": True
         },
        {"__v": 0,
         "_id": ObjectId("50f9203c210fdd671800000f"),
         "actionType": {
             "_id": ObjectId("50a4a722e4212acf562a4cf5"),
             "name": "Оформление заказа"
         },
         "currency": {
             "name": "USD"
         },
         "jscode": "http://www.yandex.ru/",
         "offer": ObjectId("50f9203c210fdd671800000b"),
         "rateadvert": "10",
         "ratepartner": "15",
         "status": True
         },
        {"__v": 0,
         "_id": ObjectId("50f9203c210fdd671800000e"),
         "actionType": {
             "_id": ObjectId("50a4a722e4212acf562a4cf5"),
             "name": "Оформление заказа"
         },
         "currency": {
             "name": "USD"
         },
         "jscode": "http://www.yandex.ru/",
                   "offer": ObjectId("50f9203c210fdd671800000c"),
                   "rateadvert": "10",
                   "ratepartner": "15",
                   "status": True
         },

        {"__v": 0,
         "_id": ObjectId("50f9203c210fdd671800000d"),
         "actionType": {
             "_id": ObjectId("50a4a722e4212acf562a4cf5"),
             "name": "Оформление заказа"
         },
         "currency": {
             "name": "USD"
         },
         "jscode": "http://www.yandex.ru/",
                   "offer": ObjectId("50f9203c210fdd671800000c"),
                   "rateadvert": "0.5%-15%",
                   "ratepartner": "0.4%-12%",
                   "status": True
         }
    ]

    currencies = [
        {"_id": ObjectId("511967a68d340044c814f814"), "name":
         "RUR", "rate": 1},
        {"_id": ObjectId(
            "511967c38d340044c814f815"), "name": "USD", "rate": 30.7209},
        {"_id": ObjectId(
            "511967d08d340044c814f816"), "name": "EUR", "rate": 40.0631}
    ]
    database.currencies.insert(currencies)
    database.statusurlpasswords.insert(password_object)
    database.tariffs.insert(tarif_object)
    database.companies.insert(campaign)
    database.offers.insert(offer)


def start():
    """Do some work before testing"""
    load_fixtures()
    start_goodvert("reciever", verbose=False)


def stop():
    """Do some work after testing"""
    stop_node_js("reciever")
    drop_test_database("reciever")
    test_dir = os.path.split(os.path.realpath(__file__))[0]
    os.chdir(test_dir)
