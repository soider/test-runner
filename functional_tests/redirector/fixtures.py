# *-* encoding: utf-8 *-*
"""This module contains some functionality, running before testsuite"""
import sys
sys.path.append("..")  # dirty hack while no setup.py
import os

from bson.objectid import ObjectId
from bson.binary import Binary
from tools.fixtures import generate_tmp_config
from tools.fixtures import start_goodvert, stop_node_js
from tools.mongo import get_test_database, drop_test_database


database = get_test_database("redirector")


def load_fixtures():
    """Inserts some test data need to correct functional cases"""
    geolocations = [{ "_id" : 2, "name" : "Каменец-Подольский", "parrentIds" : [ 3280, 3269, 3030, 3390, 3391 ], "type" : 0 },
{ "_id" : 3031, "name" : "Староконстантинов", "parrentIds" : [ 3280, 3269, 3030, 3390, 3391 ], "type" : 0 },
{ "_id" : 11, "name" : "Изяслав", "parrentIds" : [ 3280, 3269, 3030, 3390, 3391 ], "type" : 0 },
{ "_id" : 32, "name" : "Житомир", "parrentIds" : [ 3281, 3269, 3030, 3390, 3391 ], "type" : 0 },
{ "_id" : 38, "name" : "Овруч", "parrentIds" : [ 3281, 3269, 3030, 3390, 3391 ], "type" : 0 },
{ "_id" : 51, "name" : "Могилёв-Подольский", "parrentIds" : [ 3282, 3269, 3030, 3390, 3391 ], "type" : 0 },
{ "_id" : 54, "name" : "Казатин", "parrentIds" : [ 3282, 3269, 3030, 3390, 3391 ], "type" : 0 },
{ "_id" : 67, "name" : "Белая Церковь", "parrentIds" : [ 3283, 3269, 3030, 3390, 3391 ], "type" : 0 },
{ "_id" : 70, "name" : "Вишнёвое", "parrentIds" : [ 3283, 3269, 3030, 3390, 3391 ], "type" : 0 },
{ "_id" : 75, "name" : "Боярка", "parrentIds" : [ 3283, 3269, 3030, 3390, 3391 ], "type" : 0 },
{ "_id" : 80, "name" : "Украинка", "parrentIds" : [ 3283, 3269, 3030, 3390, 3391 ], "type" : 0 },
{ "_id" : 90, "name" : "Нежин", "parrentIds" : [ 3284, 3269, 3030, 3390, 3391 ], "type" : 0 },
{ "_id" : 102, "name" : "Варва", "parrentIds" : [ 3284, 3269, 3030, 3390, 3391 ], "type" : 0 },
{ "_id" : 120, "name" : "Ахтырка", "parrentIds" : [ 3285, 3269, 3030, 3390, 3391 ], "type" : 0 },
{ "_id" : 131, "name" : "Воронеж", "parrentIds" : [ 3285, 3269, 3030, 3390, 3391, 3400 ], "type" : 0 },
{ "_id" : 137, "name" : "Лубны", "type" : 0, "parrentIds" : [ 3286, 3269 ] },
{ "_id" : 154, "name" : "Черкассы", "parrentIds" : [ 3287, 3269, 3030, 3390, 3391 ], "type" : 0 },
{ "_id" : 160, "name" : "Корсунь-Шевченковский", "parrentIds" : [ 3287, 3269, 3030, 3390, 3391 ], "type" : 0 },
{ "_id" : 175, "name" : "Светловодск", "parrentIds" : [ 3288, 3269, 3030, 3390, 3391 ], "type" : 0 },
{ "_id" : 186, "name" : "Новоархангельск", "type" : 0, "parrentIds" : [ 3288, 3269 ] },
]

    offer = [{
        "__v": 2,
        "_id": ObjectId("50f544d311a1453758000020"),
        "advertiser": ObjectId("50f52c0a11a145375800001f"),
        "campaign_warning": "",
        "category": ObjectId("50a4a74ae4212acf562a4cff"),
        "conf": {
            "hasPixel": True,
            "s2s": False,
            "workScript": ""
        },
        "description": " description 41",
        "dont_track_status": False,
        "hold": "30",
        "inputUrl": "http://dont-go-here.com/{$subpage}?webmasterid=\
{$webmasterId}&source={$source}&clickId={$click_id}",
        "link": "http://www.link41",
        "logo": "/uploads/50accb44d80869ac22000002/Seafight_logo1.png",
        "name": "offer 41",
        "postclick": 365,
        "promo": "/uploads/50accb44d80869ac22000002/Seafight_logo1.rar",
        "statusUrl": "http://www.statusUrl41.ru/",
        "tariffs": [
            ObjectId("50f544d311a1453758000021")
        ],
        "trafficSourceTypes": [
            ObjectId("50a4a757e4212acf562a4d00"),
            ObjectId("50a4a757e4212acf562a4d02"),
            ObjectId("50a4a757e4212acf562a4d04"),
            ObjectId("50a4a757e4212acf562a4d05"),
            ObjectId("50a4a757e4212acf562a4d06"),
            ObjectId("50a4a757e4212acf562a4d07")
        ],
        "unhold": False,
        "visible": True,
        "warning": "warning 41"
    },{
        "__v": 2,
        "_id": ObjectId("50f544d311a1453758000021"),
        "advertiser": ObjectId("50f52c0a11a145375800001f"),
        "campaign_warning": "",
        "category": ObjectId("50a4a74ae4212acf562a4cff"),
        "conf": {
            "hasPixel": True,
            "s2s": False,
            "workScript": ""
        },
        "defaultTrafficBack": "http://offer-level-traffic.back",
        "description": " description 41",
        "dont_track_status": False,
        "geolocations": [
            3031
        ],
        "hold": "30",
        "inputUrl": "http://dont-go-here.com/{$subpage}?webmasterid=\
{$webmasterId}&source={$source}&clickId={$click_id}",
        "link": "http://www.link41",
        "locations": [
            ObjectId("50a4a739e4212acf562a4cf8"),
            ObjectId("50a4a739e4212acf562a4cf9")
        ],
        "logo": "/uploads/50accb44d80869ac22000002/Seafight_logo1.png",
        "name": "offer 41",
        "postclick": 365,
        "promo": "/uploads/50accb44d80869ac22000002/Seafight_logo1.rar",
        "statusUrl": "http://www.statusUrl41.ru/",
        "tariffs": [
            ObjectId("50f544d311a1453758000021")
        ],
        "trafficSourceTypes": [
            ObjectId("50a4a757e4212acf562a4d00"),
            ObjectId("50a4a757e4212acf562a4d02"),
            ObjectId("50a4a757e4212acf562a4d04"),
            ObjectId("50a4a757e4212acf562a4d05"),
            ObjectId("50a4a757e4212acf562a4d06"),
            ObjectId("50a4a757e4212acf562a4d07")
        ],
        "unhold": False,
        "visible": True,
        "warning": "warning 41"
    },{
        "__v": 2,
        "_id": ObjectId("50f544d311a1453758000022"),
        "advertiser": ObjectId("50f52c0a11a145375800001f"),
        "campaign_warning": "",
        "category": ObjectId("50a4a74ae4212acf562a4cff"),
        "conf": {
            "hasPixel": True,
            "s2s": False,
            "workScript": ""
        },
        "defaultTrafficBack": "http://offer-level-traffic.back",
        "description": " description 41",
        "dont_track_status": False,
        "geolocations": [
            3031
        ],
        "hold": "30",
        "inputUrl": "http://dont-go-here.com/{$subpage}?webmasterid=\
{$webmasterId}&source={$source}&clickId={$click_id}",
        "link": "http://www.link41",
        "locations": [
            ObjectId("50a4a739e4212acf562a4cf8"),
            ObjectId("50a4a739e4212acf562a4cf9")
        ],
        "logo": "/uploads/50accb44d80869ac22000002/Seafight_logo1.png",
        "name": "offer 41",
        "postclick": 365,
        "promo": "/uploads/50accb44d80869ac22000002/Seafight_logo1.rar",
        "statusUrl": "http://www.statusUrl41.ru/",
        "tariffs": [
            ObjectId("50f544d311a1453758000021")
        ],
        "trafficSourceTypes": [
            ObjectId("50a4a757e4212acf562a4d00"),
            ObjectId("50a4a757e4212acf562a4d02"),
            ObjectId("50a4a757e4212acf562a4d04"),
            ObjectId("50a4a757e4212acf562a4d05"),
            ObjectId("50a4a757e4212acf562a4d06"),
            ObjectId("50a4a757e4212acf562a4d07")
        ],
        "unhold": False,
        "visible": True,
        "warning": "warning 41"
    },
    {
        "__v": 2,
        "_id": ObjectId("50f544d311a1453758000023"),
        "advertiser": ObjectId("50f52c0a11a145375800001f"),
        "campaign_warning": "",
        "category": ObjectId("50a4a74ae4212acf562a4cff"),
        "conf": {
            "hasPixel": True,
            "s2s": False,
            "workScript": ""
        },
        "geolocations": [
            3031
        ],
        "description": " description 41",
        "dont_track_status": False,
        "hold": "30",
        "inputUrl": "http://dont-go-here.com/{$subpage}?webmasterid=\
{$webmasterId}&source={$source}&clickId={$click_id}",
        "link": "http://www.link41",
        "logo": "/uploads/50accb44d80869ac22000002/Seafight_logo1.png",
        "name": "offer 41",
        "postclick": 365,
        "promo": "/uploads/50accb44d80869ac22000002/Seafight_logo1.rar",
        "statusUrl": "http://www.statusUrl41.ru/",
        "tariffs": [
            ObjectId("50f544d311a1453758000021")
        ],
        "trafficSourceTypes": [
            ObjectId("50a4a757e4212acf562a4d00"),
            ObjectId("50a4a757e4212acf562a4d02"),
            ObjectId("50a4a757e4212acf562a4d04"),
            ObjectId("50a4a757e4212acf562a4d05"),
            ObjectId("50a4a757e4212acf562a4d06"),
            ObjectId("50a4a757e4212acf562a4d07")
        ],
        "unhold": False,
        "visible": True,
        "warning": "warning 41"
    }]
    campaign =\
        [{"__v": 0,
         "_id": ObjectId("51127e3e71f820dc19000083"),
         "name": "Диеты 25 кадр 1",
         "offer": ObjectId("5079424634fcc59bc77ef48e"),
         "owner": ObjectId("5055a5a805340e6c3b000001"),
         "status": "active",
         "trafficSource": ObjectId("50a0aff03414485243000002"),
         "type": "redirect",
         "workLink": "http://test?0.26159041514620185"
         },
         {"__v": 0,
         "_id": ObjectId("51127e3e71f820dc19000084"),
         "name": "Диеты 25 кадр 1",
         "offer": ObjectId("5079424634fcc59bc77ef48e"),
         "owner": ObjectId("5055a5a805340e6c3b000001"),
         "status": "active",
         "trafficBack": "http://campaigh-level-traffic.back",
         "trafficSource": ObjectId("50a0aff03414485243000002"),
         "type": "redirect",
         "workLink": "http://test?0.26159041514620185"
         },
         {"__v": 0,
         "_id": ObjectId("51127e3e71f820dc19000085"),
         "name": "Диеты 25 кадр 1",
         "offer": ObjectId("5079424634fcc59bc77ef48e"),
         "owner": ObjectId("5055a5a805340e6c3b000001"),
         "status": "active",
         "trafficSource": ObjectId("50a0aff03414485243000002"),
         "type": "redirect",
         "workLink": "http://test?0.26159041514620185"
         }]


    link = [{
        "_id": Binary("PP-vKWHjuf"),
        "realWorkLink": {
        "cmp": ObjectId("51127e3e71f820dc19000083"),
        "id": ObjectId("51127c7371f820dc1900007d"),
        "laying": "http://localhost",
        "offer": ObjectId("50f544d311a1453758000020"),
        "subpage": "test_subpage.html"
        }
    },
    {
        "_id": Binary("PP-vKWHjuv"),
        "realWorkLink": {
        "cmp": ObjectId("51127e3e71f820dc19000084"),
        "id": ObjectId("51127c7371f820dc1900007f"),
        "laying": "http://localhost",
        "offer": ObjectId("50f544d311a1453758000021"),
        "subpage": "test_subpage.html"
        }
    },
    {
        "_id": Binary("PP-vKWHjuj"),
        "realWorkLink": {
        "cmp": ObjectId("51127e3e71f820dc19000083"),
        "id": ObjectId("51127c7371f820dc1900007f"),
        "laying": "http://localhost",
        "offer": ObjectId("50f544d311a1453758000022"),
        "subpage": "test_subpage.html"
        }
    },
    {
        "_id": Binary("PP-vKWHjvj"),
        "realWorkLink": {
        "cmp": ObjectId("51127e3e71f820dc19000085"),
        "id": ObjectId("51127c7371f820dc1900007e"),
        "laying": "http://localhost",
        "offer": ObjectId("50f544d311a1453758000023"),
        "subpage": "test_subpage.html"
        }
    }]
    currencies = [
        {"_id": ObjectId("511967a68d340044c814f814"), "name":
         "RUR", "rate": 1},
        {"_id": ObjectId(
            "511967c38d340044c814f815"), "name": "USD", "rate": 30.7209},
        {"_id": ObjectId(
            "511967d08d340044c814f816"), "name": "EUR", "rate": 40.0631}
    ]
    database.currencies.insert(currencies)
    database.offers.insert(offer)
    database.shortlinks.insert(link)
    database.companies.insert(campaign)
    database.geolocations.insert(geolocations)


def start():
    """Do some work before testing"""
    load_fixtures()
    start_goodvert("redirector", verbose=False)


def stop():
    """Do some work after testing"""
    stop_node_js("redirector")
    drop_test_database("redirector")
    test_dir = os.path.split(os.path.realpath(__file__))[0]
    os.chdir(test_dir)
