"""This module contains some assert specific functions"""
import sys
sys.path.append("..")
import os
import pymongo
import time
import pprint
from urlparse import urlparse, urlunparse, ParseResult, parse_qsl
from urllib import urlencode

from bson.objectid import ObjectId
from bson.binary import Binary

from tools.mongo import get_test_database, drop_test_database
from tools.asserts import check_gdclick, check_gdsource, \
                          check_ai, check_apid, check_subid, \
                          check_clickid, assert_eq

p = pprint.pprint
database = get_test_database("redirector")


def test_mongo_insert(case, last_insert, short_link, location):
        case["config"]["values"]["gdsource"] = \
                        str(short_link["realWorkLink"]["cmp"])
        case["config"]["values"]["gdclick"] = \
                        str(short_link["realWorkLink"]["id"])
        case["config"]["values"]["offer_id"] = \
                        str(short_link["realWorkLink"]["offer"])
        check_gdclick(case, last_insert)
        check_gdsource(case, last_insert)
        click_id = str(last_insert["_id"])
        campaign = str(last_insert["t"])

        assert_eq(location.scheme,
                  case["config"]["values"]["scheme"],
                  "Invalid scheme")
        assert_eq(location.netloc,
                  case["config"]["values"]["location"],
                  "Invalid location")
        assert_eq(location.path,
                  case["config"]["values"]["subpage"],
                  "Invalid subpage")
        qs = {"webmasterid": case["config"]["values"]["gdclick"],
            "source": "{0}.{1}".format(campaign, click_id),
            "clickId": click_id
        }

        if "subid" in case["config"]["values"]:
            qs["source"] = \
                qs["source"] + "-{0}".format(case["config"]["values"]["subid"])

        new_qs = dict(parse_qsl(location.query))
        assert_eq(new_qs["webmasterid"],
                  qs["webmasterid"],
                  "Invalid webmaster id")
        assert_eq(new_qs["source"],
                  qs["source"],
                  "Invalid source")
        assert_eq(new_qs["clickId"],
                  qs["clickId"],
                  "Invalic clickid")


def test_traffic_back(case, result, short_link):
  """Test for case when traffic back happened"""

  # All compare values used from fixtures
  if case["config"]["trafficBackType"] == "campaign":
    assert_eq(result.headers["location"], "http://campaigh-level-traffic.back")
  if case["config"]["trafficBackType"] == "offer":
    assert_eq(result.headers["location"], "http://offer-level-traffic.back")
  if case["config"]["trafficBackType"] == "global":
    assert_eq(result.headers["location"], "http://global-level-traffic.back")


def base_assert(case):
    """Enter point"""
    try:
        time.sleep(2)
        result = case["result"]
        assert_eq(result.getcode(), 302, "Invalid response code")
        assert_eq(result.read().strip(), '', "Non empty response body")
        location = urlparse(result.headers["location"])
        last_insert = database.rawactions.find_one()
        short_link = database.shortlinks.find_one(
                  {
                      "_id": Binary(str(case["config"]["values"]["short"]))
                  }
                  )
        if short_link is None:
              raise Exception("There was no such shortlink \
  in test db, something with fixtures?")

        if case["config"].get("trafficBack", None):
          test_traffic_back(case, result, short_link)
        else:
          if last_insert is None:
              raise Exception("There was no insert in db")
          p(short_link)
          test_mongo_insert(case, last_insert, short_link, location)



    except Exception, er:
        raise er
    finally:
        database.rawactions.drop()
