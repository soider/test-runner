ENABLED_SUITES = [
        "approver",
        "canceller",
        "hold_receiver",
        "reciever",
        "s2s",
        "redirector"
        ]

SUITE_HOSTS = [
        "http://127.0.0.1:8030",
        "http://127.0.0.1:8070",
        "http://127.0.0.1:8060",
        "http://127.0.0.1:8010",
        "http://127.0.0.1:8040",
        "http://127.0.0.1:8020"
        ]

HOSTS = dict(zip(ENABLED_SUITES, SUITE_HOSTS))
