Functional tests
================

1) Для работы необходимы библиотеки python - colored, pymongo, bson
    - sudo apt-get install python-pip python-dev
    - sudo apt-get install python-pymongo
    - sudo pip install termcolor
    - sudo pip install psutil

Так же возможно установить всё через ./install.sh
Запуск: ./run_all.sh
Запуск кейсов по отдельности: python runner.py case case1 case2, например:
    python runner.py s2s reciever hold_receiver

2) Тесты хранятся в директориях <suite_name>
В каждой директории должны быть:
    - директория cases с json файлами, описаниями кейсов. (3)
    - файл __init__.py
    - файл asserts.py с функцией base_assert(case) (5)
    - файл fixtures  с функцией start и необязательной stop (6)

3) Cases
В директории должны быть конфигурации testcase для каждого testsuite
Формат имени <integer>.json

    {
        "description": <string>,
        "values": <hash>,
        "query": <hash>,
        "should_fail": boolean
    }

description - строка, описание кейса, используется в отчетах.
values - словарь, значения из которого используются при построении url (см. 4) и при проверке в asserts (см. 5)
query - словарь, описывающий построение query_string для запроса
should_fail - необязательный параметр, если true, то провал case рассматривается как успех.

Так как соответствующий конфиг всегда передается в assert функции в case["config"], можно добавлять кастомные ключи.

4) Построение url
Генерация тестового query_string происходит на основе query-хеша.
Пример:

    "query": {
        "ai": "@ai",
        "apid": "@apid",
        "hash": "@hash",
        "gdclick": "@gdclick",
        "gdsource": {
            "template": "{0}.{1}-{2}",
            "subtitutes": ["@gdsource", "@clickId", "@subid"]
        }

Каждый ключ из query-хеша является параметром в query_string'e.
Каждое значение ключа либо строка, либо значение из values, если начинается с "@" (см. пример), либо шаблонный объект (cм. пример)
Шаблонный объект
    {
        "template": string,
        "subtitutes": list of strings
    }
template - формат строка для String.format
subtites - список строк-подстановок для String.format

Пример:
    {
    "description": "Example",
    "values": {
        "param1": "Param1Value",
        "param2": "Param2Value",
        "param3": "Param3Value"
    },
    "query": {
        "query_param1": "@param1",
        "query_param2": "@param2",
        "query_param3": "fixedvalue",
        "query_param4": {
            "template": "{0}.{1}-{2}",
            "subtitutes": ["@gdsource", "fixed", "string"]
        }
    }
}

Query_string: query_param1=Param1Value&query_param2=Param2Value&query_param3=fixedvalue&query_param4=Param3Value.fixed-string

5) Модуль asserts.

В модуле asserts должны быть реализованы функции проверки корректности case'ов.
После запуска каждого case, запускается asserts.base_assert(case), где case - словарь, описывающий результаты кейса
{
    "title": string,
    "idx": int,
    "config": hash,
    "result": fileobject
}
config - конфиг из json-описания case
result - результат работы urllib2.urlopen

6) Модуль fixtures.

Функция start выполняется перед запуском первого case
Функция stop выполняется после запуска последнего case

7) Tools

В пакете tools лежат общие для всех кейсов функции


8) Тестирование через vagrant

Vagrant:
http://vagrantup.com

Virtualbox:
https://www.virtualbox.org/

Для тестирования в виртуальной системе необходимо установить vagrant, после чего в директории goodvertbase/test/functional_tests выполнить следующие команды:
 - vagrant up # установит виртуальную машину с Ubuntu linux
 - vagrant ssh # ssh подключение в виртуальную машину
 - cd goodvert/test/functional_tests # в директории goodvert смонтировано содержимое goodvertbase/test/functional_tests
 - ./install_env.sh # установка node js и mongo
 - ./install.sh
 - ./run_all.sh