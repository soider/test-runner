"""Module implements some cases config parsing specific functions"""

import urllib


def url_parser(base_url, case):
    """Read case config and return case specifig url

    @param `base_url` - test suite base url
    @param `case` - dict with case environment

    @return string"""
    config_query_params = case["config"]["query"]
    values = case["config"]["values"]
    query_params = {}
    for key, value in config_query_params.items():
        if type(value) == dict:
            query_params[key] = parse_template(value, values)
        else:
            query_params[key] = parse_line(value, values)
    return "{0}?{1}".format(
                            base_url,
                            urllib.urlencode(query_params)
                            )


def parse_line(line, values):
    """Parse line

    If line like "@key", use values[key] instead of line
    Else use exact line

    @param `line` - line to parse_line
    @param `values` - dict with subtitutes

    @return string"""
    if line.startswith("@"):
        return values[line[1:]]
    else:
        return line


def parse_template(template, values):
    """
    Parse template object from config

    @param `template` - dict from json config with declaration of parameter
    @values - dict with subtitutes

    @return formatted string
    """
    subtitutes = [parse_line(sub, values)
                        for sub in template["subtitutes"]]
    format_string = template["template"]
    return format_string.format(*subtitutes)
