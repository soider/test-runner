"""Some tools using inside fixtures,
THREADS UNSAFE"""
import os
import tempfile
import subprocess
import time

from .utils import is_same_running
from .exceptions import CanNotStart


def generate_tmp_config(app_path, name):
    """Parses real application config
    and changes mongodb connectors,
    saves new config in NamedTemporaryFile

    @param `app_path` - application directory
    @return string temporary file path"""
    pid = os.getpid()
    config = open(os.path.join(app_path,
                               "settings",
                               "development.js")).readlines()
    new_config = []
    for line in config[:]:
        if line.strip().startswith("uri:"):
            new_config.append("uri: 'mongodb://localhost:27017/test_{0}_{1}\
?socketTimeoutMS=300000;auto_reconnect=true',\n".format(name, pid))
        elif line.strip().startswith("workdb:"):
            new_config.append("workdb: 'mongodb://localhost:27017/test_{0}_{1}\
?safe=false;journal=false;j=false;\
auto_reconnect=true;socketTimeoutMS=300000'\n".format(name, pid))
        elif line.strip().startswith("batchWaitingTime:"):
            new_config.append("batchWaitingTime: 1,")
        elif line.strip().startswith("defaultTrafficBack:"):
            new_config.append("defaultTrafficBack: 'http://global-level-traffic.back',")
        else:
            new_config.append(line)
    tmp_config = tempfile.NamedTemporaryFile(delete=False)
    tmp_config.write("".join(new_config))
    return tmp_config.name


node_processes = {}


def start_node_js(config, node_dir, worker, verbose=False):
    """Do some work before testing

    @param `config` - node js config
    @param `node_dir` - project root directory
    @param `worker` - worker name, used as key in dictionary
    @param `verbose` - if True, send output to stdout """
    global node_process
    print "Starting node js process"
    args = [
        "/usr/local/bin/node",
        os.path.join(node_dir, "server.js"),
        "-w",
        worker,
        "-c",
        config
    ]
    if is_same_running(args):
        raise CanNotStart("{0} already running".format(
                " ".join(args[:-1]))
        )
    if verbose:
        node_processes[worker] = \
        subprocess.Popen(args)
    else:
        node_processes[worker] = \
        subprocess.Popen(args,
                         stdout=open("/tmp/test_out_{0}_{1}"\
                            .format(os.getpid(), worker), "w"))
    print "Sleep 10 seconds (waiting for node start)"
    time.sleep(10)


def stop_node_js(worker):
    """Do some work after testing

    @param `worker` - worker name, key in processes dict"""
    global node_process
    print "Stopping node js process"
    node_processes[worker].kill()


def start_goodvert(worker, verbose=False):
    """Wrapper arround `start_node_js`

    @param `verbose` - if True, send output to stdout
    @param `worker` - worker name, used as key in dictionary
    """
    test_dir = os.path.split(os.path.realpath(__file__))[0]
    node_app = os.path.normpath(os.path.join(test_dir, "..", "..", ".."))
    os.chdir(node_app)
    config = generate_tmp_config(node_app, worker)
    start_node_js(config, node_app, worker, verbose)
