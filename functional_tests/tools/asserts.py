"""Module implements some basic asserts functions for all goodverts tests"""
import hashlib
from bson.objectid import ObjectId
import pprint
p = pprint.pprint

from decimal import Decimal


def assert_eq(fst, snd, msg=None):
    """Simple wrapper arount assert instruction"""
    if msg is None:
        msg = ""
    else:
        msg = "{0} :".format(msg)
    try:
        assert fst == snd
    except AssertionError:
        raise AssertionError("{0}{1} != {2}".format(msg, fst, snd))


def check_ai(case, last_insert):
    """Check correctness of ai"""
    oid = ObjectId(case["config"]["values"]["ai"])
    assert_eq(oid, last_insert["a"], "Invalid ai parameter in database")


def check_apid(case, last_insert):
    """Check correctness of apid"""
    assert_eq(int(case["config"]["values"]["apid"]),
              last_insert["oi"]["a"],
              "Invalid apid (obj.oi.a)")


def check_gdclick(case, last_insert):
    """Check gdclick_parameter"""
    oid = ObjectId(case["config"]["values"]["gdclick"])
    assert_eq(oid, last_insert["c"], "Invalid gdclick (obj.c)")


def check_gdsource(case, last_insert):
    """Check correctness of gdsource"""
    oid = ObjectId(case["config"]["values"]["gdsource"])
    assert_eq(oid, last_insert["t"], "Invalid gdsource (obj.t)")


def check_subid(case, last_insert):
    """Check correctness of subid"""
    assert_eq(case["config"]["values"]["subid"],
              str(last_insert["f"]),
              "Invalid subid (obj.f)")


def check_clickid(case, last_insert):
    """Check correctness of clickid"""
    oid = ObjectId(case["config"]["values"]["clickId"])
    assert_eq(oid, last_insert["cl"], "Invalid clickid (obj.cl)")


def is_hash_correct(case, database):
    """Check password hash correctness"""
    ai = case["config"]["values"]["ai"]
    tariff = database.tariffs.find_one({"_id": ObjectId(ai)})
    password = database.statusurlpasswords.find_one({"_id": tariff["offer"]})
    password_value = password["passw"]
    if not password_value:
        return False
    return case["config"]["values"]["hash"] == \
        hashlib.md5(
            case["config"]["values"]["apid"] + password_value).hexdigest()


def is_pass_correct(case, database):
    """Check password pass correctness"""
    ai = case["config"]["values"]["ai"]
    tariff = database.tariffs.find_one({"_id": ObjectId(ai)})
    password = database.statusurlpasswords.find_one({"_id": tariff["offer"]})
    password_value = password["passw"]
    if not password_value:
        return False
    return case["config"]["values"]["pass"] == \
        hashlib.md5(
            case["config"]["values"]["apid"] + password_value).hexdigest()


def check_price_with_percent(percent_adv, percent_partner,
                             rate, price, last_insert):
    pr_a = Decimal(price * (percent_adv / 100) * rate)
    pr_p = Decimal(price * (percent_partner / 100) * rate)
    assert_eq(round(pr_a, 2), last_insert["pr"]["a"],
              "Invalid price in pr a with percent")
    assert_eq(round(pr_p, 2), last_insert["pr"]["p"],
              "Invalid price in pr p with percent")


def check_price_with_flatten_multiplier(adv_multiplier, partner_multiplier,
                                        rate, price, last_insert):
    pr_a = Decimal(adv_multiplier * rate)
    pr_p = Decimal(partner_multiplier * rate)
    assert_eq(round(pr_a, 2), last_insert["pr"]["a"],
              "Invalid price in pr a with flat price")
    assert_eq(round(pr_p, 2), last_insert["pr"]["p"],
              "Invalid price in pr p with flat price")


def price_check(price_param, case, last_insert, tariff, currencies):
    rate = float([currency for currency in currencies if currency
                  ["name"] == tariff["currency"]["name"]][0]["rate"])
    price = float(case["config"]["values"][price_param])
    oi_original = last_insert["oi"].get("o", None)
    if oi_original:
        assert_eq(price,
                  float(last_insert["oi"]["o"]),
                  "Invalid price in oi.o")
        assert_eq(round(Decimal(price * rate), 2),
                  round(Decimal(last_insert["oi"]["p"]), 2),
                  "Invalid price in oi.p")
    else:
        assert_eq(price,
                  float(last_insert["oi"]["p"]),
                  "Invalid price in oi.p")
    if "%" in tariff["rateadvert"]:
        percent_adv = float(tariff["rateadvert"].split("%")[0])
        percent_partner = float(tariff["ratepartner"].split("%")[0])
        check_price_with_percent(
            percent_adv, percent_partner, rate, price, last_insert)
    else:
        adv_multiplier = float(tariff["rateadvert"])
        partner_multiplier = float(tariff["ratepartner"])
        check_price_with_flatten_multiplier(
            adv_multiplier, partner_multiplier, rate, price, last_insert)


def check_price_and_currency(case, last_insert, tariff, currencies):
    price_check("price", case, last_insert, tariff, currencies)


def check_aprice_and_currency(case, last_insert, tariff, currencies):
    price_check("aprice", case, last_insert, tariff, currencies)
