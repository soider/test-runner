import os
import urllib2
import urllib

from string import maketrans

import psutil


def is_same_running(args):
    """Check, is process running

    @param `args` - popen args"""

    (node, server, option_worker,
     worker_type, option_config,
     config) = args
    prs = []
    for pr in psutil.get_process_list():
        try:
            prs.append(pr.cmdline)
        except:
            pass
    for process in prs:
        if worker_type in process:
            for proc_args in process:
                if "node" in proc_args.lower():
                    return True
    return False


def get_no_redirect_opener():
    """Build opener which not follow redirects

    @return urllib2.opener.open"""

    class NoRedirectHandler(urllib2.HTTPRedirectHandler):

        def http_error_302(self, req, fp, code, msg, headers):
            infourl = urllib.addinfourl(fp, headers, req.get_full_url())
            infourl.status = code
            infourl.code = code
            return infourl

        http_error_300 = http_error_302
        http_error_301 = http_error_302
        http_error_303 = http_error_302
        http_error_307 = http_error_302

    return urllib2.build_opener(NoRedirectHandler())
