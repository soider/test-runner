"""Some mongo tests specific functions
THREADS UNSAFE!"""
import pymongo
import os

connect = pymongo.Connection()


def get_test_database(name):
    """Return mongo db collection, name based on current pid

    @param `name` - string
    @returns mongodb-collection pointer
    """
    pid = os.getpid()
    return connect["test_{0}_{1}".format(name, pid)]


def drop_test_database(name):
    """Delete mongo db test collection"""
    pid = os.getpid()
    connect.drop_database("test_{0}_{1}".format(name, pid))
