"""Module implements some exceptions for runner"""


class CanNotStart(Exception):
    pass
