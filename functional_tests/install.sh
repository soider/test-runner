#!/bin/bash
echo "Will install test dependencies\n"
sudo apt-get install python-pip &&
sudo apt-get install python-dev &&
sudo apt-get install python-pymongo &&
sudo pip install termcolor &&
sudo pip install psutil
