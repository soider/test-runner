# *-* encoding: utf-8 *-*
"""This module contains some functionality, running before testsuite"""
import sys
sys.path.append("..")  # dirty hack while no setup.py

import os
import json
import urllib2

from time import sleep
from bson.objectid import ObjectId
from tools.mongo import get_test_database, drop_test_database
from tools.fixtures import start_goodvert, start_node_js, stop_node_js
from tools.fixtures import generate_tmp_config
from tools.config_parser import url_parser

database = get_test_database("approver")


def load_fixtures():
    """Inserts some test data need to correct functional cases"""
    password_object = [{"_id": ObjectId("50f9203c210fdd671800000c"),
                       "passw": "testtest"},
                       {"_id": ObjectId("50f9203c210fdd671800000b"),
                       "passw": None}]
    tarif_object = [{"__v": 0,
                    "_id": ObjectId("50f9203c210fdd671800000e"),
                    "actionType": {
                        "_id": ObjectId("50a4a722e4212acf562a4cf5"),
                        "name": "Оформление заказа"
                    },
        "currency": {
        "name": "USD"
        },
        "jscode": "http://www.yandex.ru/",
        "offer": ObjectId("50f9203c210fdd671800000c"),
        "rateadvert": "10",
        "ratepartner": "15",
        "status": True
    },
    {"__v": 0,
                    "_id": ObjectId("50f9203c210fdd671800000f"),
                    "actionType": {
                        "_id": ObjectId("50a4a722e4212acf562a4cf5"),
                        "name": "Оформление заказа"
                    },
        "currency": {
        "name": "USD"
        },
        "jscode": "http://www.yandex.ru/",
        "offer": ObjectId("50f9203c210fdd671800000b"),
        "rateadvert": "10",
        "ratepartner": "15",
        "status": True
    },

        {"__v": 0,
         "_id": ObjectId("50f9203c210fdd671800000d"),
         "actionType": {
             "_id": ObjectId("50a4a722e4212acf562a4cf5"),
             "name": "Оформление заказа"
         },
         "currency": {
             "name": "USD"
         },
         "jscode": "http://www.yandex.ru/",
                   "offer": ObjectId("50f9203c210fdd671800000c"),
                   "rateadvert": "0.5%-15%",
                   "ratepartner": "0.4%-12%",
                   "status": True
         }
    ]
    campaign =\
        {"__v": 0,
         "_id": ObjectId("5100eed2f7eacf0a75000027"),
         "name": "Диеты 25 кадр 1",
         "offer": ObjectId("5079424634fcc59bc77ef48e"),
         "owner": ObjectId("5055a5a805340e6c3b000001"),
         "status": "active",
         "trafficBack": "http://ya.ru",
         "trafficSource": ObjectId("50a0aff03414485243000002"),
         "type": "redirect",
         "workLink": "http://test?0.26159041514620185"
         }
    offer = {
        "__v": 2,
        "_id": ObjectId("50f9203c210fdd671800000c"),
        "advertiser": ObjectId("50f52c0a11a145375800001f"),
        "campaign_warning": "",
        "category": ObjectId("50a4a74ae4212acf562a4cff"),
        "conf": {
            "hasPixel": True,
            "s2s": False,
            "workScript": ""
        },
        "description": " description 41",
        "dont_track_status": False,
        "geolocations": [
            3031,
            3030
        ],
        "hold": "30",
        "inputUrl": "http://dont-go-here.com/{$subpage}?webmasterid=\
{$webmasterId}&source={$source}&clickId={$click_id}",
        "link": "http://www.link41",
        "locations": [
            ObjectId("50a4a739e4212acf562a4cf8"),
            ObjectId("50a4a739e4212acf562a4cf9")
        ],
        "logo": "/uploads/50accb44d80869ac22000002/Seafight_logo1.png",
        "name": "offer 41",
        "postclick": 365,
        "promo": "/uploads/50accb44d80869ac22000002/Seafight_logo1.rar",
        "statusUrl": "http://www.statusUrl41.ru/",
        "tariffs": [
            ObjectId("50f544d311a1453758000021")
        ],
        "trafficSourceTypes": [
            ObjectId("50a4a757e4212acf562a4d00"),
            ObjectId("50a4a757e4212acf562a4d02"),
            ObjectId("50a4a757e4212acf562a4d04"),
            ObjectId("50a4a757e4212acf562a4d05"),
            ObjectId("50a4a757e4212acf562a4d06"),
            ObjectId("50a4a757e4212acf562a4d07")
        ],
        "unhold": False,
        "visible": True,
        "warning": "warning 41"
    }
    currencies = [
        {"_id": ObjectId("511967a68d340044c814f814"), "name":
         "RUR", "rate": 1},
        {"_id": ObjectId(
            "511967c38d340044c814f815"), "name": "USD", "rate": 30.7209},
        {"_id": ObjectId(
            "511967d08d340044c814f816"), "name": "EUR", "rate": 40.0631}
    ]
    database.currencies.insert(currencies)
    database.statusurlpasswords.insert(password_object)
    database.tariffs.insert(tarif_object)
    database.companies.insert(campaign)
    database.offers.insert(offer)


def interact_with_hold():
    """Start hold_receiver application
    and creating some fixture data based on hold_receiver cases
    """
    test_dir = os.path.split(os.path.realpath(__file__))[0]
    node_app = os.path.normpath(os.path.join(test_dir, "..", "..", ".."))
    os.chdir(node_app)
    config = generate_tmp_config(node_app, "approver")
    start_node_js(config, node_app, "hold_receiver")
    cur_dir = os.path.split(os.path.realpath(__file__))[0]
    case_dir = os.path.normpath(
        os.path.join(
            cur_dir, "..", "hold_receiver", "cases"
        )
    )
    cases = [os.path.join(case_dir, f) for f in os.listdir(case_dir)
             if f.endswith("json")]
    cases = [
        {"idx": idx + 1,
            "name": case,
            "config": json.load(open(case))
             }
        for idx, case in enumerate(cases)
    ]
    urls = [url_parser("http://127.0.0.1:8060", case) for case in cases]
    for url in urls:
        urllib2.urlopen(url)
    stop_node_js("hold_receiver")


def start():
    """Do some work before testing"""
    load_fixtures()
    interact_with_hold()
    start_goodvert("approver", verbose=False)


def stop():
    """Do some work after testing"""
    drop_test_database("approver")
    stop_node_js("approver")
