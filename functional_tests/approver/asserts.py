import sys
sys.path.append("..")
import os
import pymongo
import time
import pprint
from bson.objectid import ObjectId

from tools.mongo import get_test_database, drop_test_database
from tools.asserts import check_gdclick, check_gdsource, check_ai,\
                          check_apid, check_subid, check_clickid,\
                          check_subid, is_pass_correct, assert_eq

p = pprint.pprint
database = get_test_database("approver")


def check_status(action, case):
    assert_eq(action["ns"], -1)
    apid = int(case["config"]["values"]["apid"])
    actionID = ObjectId(case["config"]["values"]["ai"])
    assert_eq(action["oi"]["r"]["status"],
              1,
              "Invalid status (oi.r.status)")
    assert_eq(action["oi"]["r"]["offerId"],
              apid,
              "Invalid offerid (obj.oi.r.offerId)")
    assert_eq(action["oi"]["r"]["ai"],
              actionID,
              "Invalid actionid (obj.oi.r.ai)")


def base_assert(case):
    """Enter point"""
    query = {
        "a": ObjectId(case["config"]["values"]["ai"]),
        "c": ObjectId(case["config"]["values"]["gdclick"]),
        "oi.a": int(case["config"]["values"]["apid"])
    }
    try:
        time.sleep(2)
        if is_pass_correct(case, database):
            result = case["result"]
            assert_eq(result.headers["Content-Type"],
                      "image/png",
                      "Invalid content-type")
            assert_eq(result.getcode(), 200, "Invalid response code")
            assert_eq(result.read().strip(), '', "Non empty response body")
            result = database.rawactions.find_one(query)
            if result is None:
                raise Exception("No such action in rawactions")
            check_status(result, case)
        else:
            print "Got invalid hash, check no result"
            last_insert = database.rawactions.find_one(query)
            if last_insert is not None and last_insert["s"] == -1:
                raw_input()
                raise Exception("Inserted data with invalid hash!!!")
    except Exception, er:
        raise er
    finally:
        # Revert status back
        database.rawactions.update(query, {"$set": {"s": 0, "ns": None}})
