import os
import sys
import json
import urllib2
import traceback

import settings

from termcolor import colored

from tools.config_parser import url_parser
from tools.exceptions import CanNotStart
from tools.utils import get_no_redirect_opener


class TestSuite(object):
    """Simple functional tests suite"""

    def __init__(self, title, cases, asserts,
                 fixtures, base_url, opener_fabric):
        """ Constructor

        @param `title` - used for report name
        @param `cases` - list with path to cases
        @param `asserts` - assert function
        @param `base_url` - service root url
        @param `opener_fabric` - smth that returns urllib2.opener
        """
        self.title = title
        self.cases = [
            {"idx": idx + 1,
             "name": case,
             "config": json.load(open(case))
             }
            for idx, case in enumerate(cases)
        ]
        self.asserts = asserts
        self.fixtures = fixtures
        self.base_url = base_url
        self.opener_fabric = opener_fabric

    def make_urls(self):
        """Make url for each case from json config"""
        for case in self.cases:
            case["url"] = url_parser(self.base_url, case)

    def add_headers_to_opener(self, opener, headers):
        """Add some http-headers to opener

        @param `opener` - urllib2.opener
        @param `headers` - dict like object"""

        opener.addheaders = \
            opener.addheaders + headers.items()

    def run(self):
        """Run this suite"""
        print "Running suite for '{0}'".format(self.title)
        self.make_urls()
        self.run_cases()

    def run_cases(self):
        """Run each case"""
        self.errors = []
        self.success = []
        if self.fixtures is not None:
            self.fixtures.start()
        try:
            for case in self.cases:
                print "Running case #{0} - {1}".format(case["idx"],
                                                       case["name"])
                print "Description: "
                print case["config"]["description"]
                print case["url"]
                opener = self.opener_fabric()
                if case["config"].has_key("headers"):
                     self.add_headers_to_opener(opener, case["config"]["headers"])
                case["result"] = opener.open(
                    case["url"]
                )
                try:
                    self.asserts.base_assert(case)
                    if self.is_should_fail(case):
                        case["config"]["should_fail"] = False
                        raise \
                            Exception("Test need to be failed, but it succeed")
                except Exception, err:
                    if self.is_should_fail(case):
                        print "Test failed, but has should fail flag"
                        self.ok(case)
                    else:
                        self.fail(case)
                else:
                    self.ok(case)
                print "-" * 10
            print "Successed: {0}".format(len(self.success))
            print "Failed: {0}".format(len(self.errors))
        finally:
            if self.fixtures is not None and\
                    hasattr(self.fixtures, "stop"):
                self.fixtures.stop()

    def is_should_fail(self, case):
        """Check, should this case fail

        @param `case` - dict with case environment
        @returns boolean"""
        return "should_fail" in case["config"] \
            and case["config"]["should_fail"] == True

    def fail(self, case):
        """Handle case as fail

        @param `case` - dict with case environment"""
        self.errors.append(case)
        self.print_error(case, traceback.format_exc())

    def ok(self, case):
        """Handle case as success

        @param `case` - dict with case environment"""
        self.success.append(case)
        print colored("OK!", "green")

    def print_error(self, case, trace):
        """Print cases reports

        @param `case` - dict with case environment
        @param `trace` - string with traceback"""
        print colored("==ERROR in  case {0}".format(case["name"]), "red")
        print colored("==URL: {0}".format(case["url"]), "yellow")
        print trace


def get_test_suite(module, cases, asserts, fixtures, base_url):
    """Return specified classsuite for `module`

    @param `module` - module to test
    @returns - class object
    """
    if module != "redirector":
        return TestSuite(module, cases, asserts, fixtures, base_url, urllib2.build_opener)
    else:
        return TestSuite(module, cases, asserts,
                         fixtures, base_url,
                         get_no_redirect_opener)


def get_base_url(suite):
    """Get base url

    @param `module` - module to test
    @returns - string
    """
    try:
        return settings.HOSTS[module]
    except KeyError:
        raise CanNotStart("No settings for {0}".format(module))


def build_suite(module):
    """Create and return `TestSuite` for `module`

    @param `module` - module to test
    """
    try:
        cases = [os.path.join(".", module, "cases", f) for f in os.listdir(
            os.path.join(".", module, "cases")
        ) if f.endswith("json")]
    except OSError:
        print "No such module: {0}".format(module)
        sys.exit(1)

    cases.sort(key=lambda x:
               int(os.path.basename(x)[:-5]))  # sort by <num>.json
    asserts = __import__("{0}.asserts".format(module)).asserts
    try:
        fixtures = __import__("{0}.fixtures".format(module)).fixtures
    except ImportError:
        print "No fixtures found"
        fixtures = None
    base_url = get_base_url(module)
    return get_test_suite(module,
                          cases,
                          asserts,
                          fixtures,
                          base_url)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "USAGE: python {0} <TEST_CASE> [<TEST_CASE>]".format(sys.argv[0])
        sys.exit(1)
    suites = [build_suite(module) for module in sys.argv[1:]]
    for suite in suites:
        try:
            suite.run()
        except CanNotStart, er:
            print "{0} will not start because:\n{1}".format(
                suite.title,
                str(er)
            )
    errors_count = sum([len(suite.errors) for suite in suites])
    print ""
    print "Final report"
    print "-" * 10
    for suite in suites:
        print "Suite: {0}".format(suite.title)
        print "Errors: {0}".format(len(suite.errors))
        print "OK: {0}".format(len(suite.success))
        print "-" * 5
    print ""
    print "Total errors: {0}".format(errors_count)
    print "Total OK: {0}".format(sum([len(suite.success) for suite in suites]))
    if errors_count > 0:
        print colored("Some suites failed", "red")
        sys.exit(1)
